NULL =

SOURCES = $(wildcard src/*.hx)

SVG_SOURCES = \
	bartender \
	hardboiled \
	informant \
	umatantei \
	$(NULL)
ATLAS_SOURCES = $(patsubst %,res/%.png,$(SVG_SOURCES))
TILES_ATLAS = res/tiles.atlas

FONT_SOURCE = src/Amatic-Bold.ttf
FONT_PNG = res/AmaticSC.png
FONT_FNT = $(patsubst %.png,%.fnt,$(FONT_PNG))
FONT_STAMP = $(patsubst %.png,%.stamp,$(FONT_PNG))
.INTERMEDIATE: $(FONT_STAMP)

RESOURCES = \
	$(TILES_ATLAS) \
	$(FONT_PNG) \
	$(FONT_FNT) \
	$(NULL)

HAXE = haxe
HAXEHXML = umatantei.hxml
HAXESERVER = --connect 6000
HAXEJSHXML = umatantei.js.hxml
JSBIN = bin/umatantei.js
HAXEHLHXML = umatantei.hl.hxml
HLBIN = bin/umatantei.hl
ZIPBIN = bin/umatantei.zip

ZIP = zip
ZIPFLAGS = -9 -j

INKSCAPE = inkscape
INKSCAPEFLAGS = --export-id-only --export-type="png"

LSSVGIDS = xml select --template --value-of '/svg:svg/svg:g/svg:*/@id'


$(HLBIN): $(HAXEHLHXML) $(HAXEHXML) $(SOURCES) | $(RESOURCES)
	$(HAXE) $(HAXESERVER) --hl $@ $< || $(HAXE) --hl $@ $< 

$(JSBIN): $(HAXEJSHXML) $(HAXEHXML) $(SOURCES) | $(RESOURCES)
	$(HAXE) $(HAXESERVER) --js $@ $< || $(HAXE) --js $@ $<

$(ZIPBIN): $(JSBIN) bin/index.html
	$(ZIP) $(ZIPFLAGS) $@ $^

all: $(HLBIN) $(JSBIN) dist

run: $(HLBIN)
	hl --hot-reload $(HLBIN)

dist: $(ZIPBIN)

define ATLAS_template =
$1_SVG = src/$1.svg
$1_IDS := $$(shell $$(LSSVGIDS) $$($1_SVG))
$1_PNG := $$(patsubst %,src/$1_%.png,$$($1_IDS))
$1_STAMP := $$(patsubst %.svg,%.stamp,$$($1_SVG))
$1_ATLAS = res/$1.png 

$$($1_PNG): $$($1_STAMP)

$$($1_ATLAS): $$($1_PNG)
	packtextures $$^ $$@

.INTERMEDIATE: $$($1_PNG) $$($1_STAMP)
endef

$(foreach _,$(SVG_SOURCES),$(eval $(call ATLAS_template,$_)))

%.stamp: %.svg
	$(INKSCAPE) $(INKSCAPEFLAGS) --export-id="$$($(LSSVGIDS) $< | tr -s '\r\n' ';')" $<
	touch $@

$(TILES_ATLAS): $(ATLAS_SOURCES)
	echo $(ATLAS_SOURCES)
	extractatlas $^ $@

$(FONT_FNT) $(FONT_PNG): $(FONT_STAMP)
$(FONT_STAMP): fonts.json $(FONT_SOURCE)
	fontgen $<
	touch $@

clean:
	rm -f $(JSBIN)
	rm -f $(HLBIN)
	rm -f $(ATLAS_SOURCES)
	rm -f $(RESOURCES)

watch:
	while true; do inotifywait --recursive --event modify src; $(MAKE); done

.PHONY: all clean watch dist run