import h2d.Text;
import h2d.Scene;
import h2d.Font;
import hxd.Event;
import hxd.Res;

class Informant extends Stage {
	public function new(font:Font, parent:Scene, firstTime = true) {
		super(font, parent);
		this.firstTime = firstTime;
	}

	override function setup(dt:Float):Bool {
		super.setup(dt);

		final street = new AtlasBitmap("informant_street", root);
		street.setPosition(parent.width / 2, street.tile.height);

		final nextArrow = new AtlasBitmap("informant_next");
		final arrowInteractive = newInteractive(nextArrow.tile.width, nextArrow.tile.height);
		arrowInteractive.setPosition(620, 10);
		arrowInteractive.onPush = nextStage;
		arrowInteractive.visible = false;
		arrowInteractive.addChild(nextArrow);
		nextArrow.setPosition(-nextArrow.dx, -nextArrow.dy);

		setPosition(new AtlasBitmap("informant_light", root), 540, 88);
		umaLight = setPosition(new AtlasBitmap("umatantei_light", root), 25, 115);

		informant = new Actor("informant", root);
		informant.playAnim("fishy");
		setPosition(informant, 595, 175);
		final informantInteractive = newInteractive(informant.width + 20, informant.height + 20);
		informantInteractive.setPosition(595 - 10, 175 - 10);
		informantInteractive.onPush = talkToInformant;
		informantInteractive.visible = false;

		uma = new Actor("umatantei", root);
		uma.playAnim("standing");
		setPosition(uma, 70, 170);
		xDiff = umaLight.x - uma.x - uma.dx;

		if (!firstTime) {
			uma.x = parent.width + uma.width;
			syncLight();
		}

		createFade();

		run(fadeIn);
		if (firstTime) {
			hxd.snd.Manager.get().stopAll();
			Res.bass_walker.play(true);

			umaSay("Better start digging around.");
			umaSay("The case is not going to solve by itself.");
			umaSay("I wish.");
			wait(0.5);
			umaSay("Let’s see what I can fish out.");
			run(enableInteractives);
			run(returnControl);
		} else {
			run(walkFromWest);
			run(enableInteractives);
		}

		return false;
	}

	function umaSay(line:String) {
		run((dt) -> say(uma.x, uma.y - uma.height, line));
	}

	function informantSay(line:String) {
		run((dt) -> say(informant.x, informant.y - informant.height, line));
	}

	function talkToInformant(e:Event) {
		run(walkToInformant);
		run(disableInteractives);
		run(faceInformant);
		if (Main.ME.state.talkToSardinian && Main.ME.state.talkToHardboiled) {
			informantSay("There was no time to\nadd more content.");
			umaSay("What?");
			informantSay("72 hours aren’t that\nmuch, you know?");
			umaSay("So, I’ve been just\nhorsing around?");
			informantSay("Well, yes.");
			wait(0.5);
			informantSay("You ARE a horse,\nDetective.");
			wait(0.5);
			umaSay("But...");
			informantSay("Shh!");
			informantSay("Don’t beat a dead horse.");
			umaSay("Chicken.");
			informantSay("Come again?");
			umaSay("The victim was a chicken.\nAnd was stabbed, not beaten.");
			informantSay("No, I meant...");
			run(fadeOut);
			wait(0.5);
			informantSay("Nevermind.");
			run(showSorry);
		} else if (Main.ME.state.talkToSardinian) {
			umaSay("Heard anything else?");
			informantSay("Maybe it’s time to\n“poach an egg”");
			umaSay("What?");
			wait(1);
			informantSay("Nevermind.");
			run(enableInteractives);
			run(returnControl);
		} else {
			Main.ME.state.talkToSardinian = true;
			umaSay("Heard anything interesting lately?");
			informantSay("Lots.");
			informantSay("We, Sardinians, are\nwell informed.");
			informantSay("Could you be more specific?");
			umaSay("I’m talking about the\nchicken’s murder.");
			wait(0.5);
			informantSay("I know of someone who’s\n“chocked the chicken”.");
			informantSay("Is that what you’re looking for?");
			wait(0.5);
			umaSay("No.");
			umaSay("The autopsy said he was stabbed.");
			wait(0.5);
			informantSay("He may also “petted the cat”.");
			wait(0.5);
			umaSay("What? You think Llyod\nis somehow involved?");
			wait(1);
			informantSay("Nevermind.");
			run(walkAwayFromInformant);
			umaSay("I don’t know...");
			wait(0.5);
			umaSay("He looks fishy to me.");
			run(enableInteractives);
			run(returnControl);
		}
	}

	function walkToInformant(dt:Float):Bool {
		return walkTo(dt, informant.x - informant.width - 10);
	}

	function walkAwayFromInformant(dt:Float):Bool {
		return walkTo(dt, informant.x - informant.width - 50);
	}

	function walkWest(dt:Float):Bool {
		return walkTo(dt, parent.width + uma.width + 10);
	}

	function walkFromWest(dt:Float):Bool {
		return walkTo(dt, parent.width - 10);
	}

	function walkTo(dt:Float, x:Float):Bool {
		if (Math.abs(uma.x - x) < 5) {
			uma.playAnim("standing");
			return false;
		}
		if (uma.currentAnim != "walking") {
			uma.playAnim("walking");
			if (uma.x < x && uma.dir < 0 || uma.x > x && uma.dir > 0) {
				uma.flipX();
			}
		}
		uma.x += 150 * dt * uma.dir;
		syncLight();
		return true;
	}

	inline function syncLight() {
		umaLight.x = uma.x + uma.dx + xDiff + (uma.dir < 0 ? 10 : 0);
	}

	function faceInformant(dt:Float):Bool {
		if (uma.dir < 0) {
			uma.flipX();
		}
		return false;
	}

	function nextStage(e:Event) {
		run(walkWest);
		run(disableInteractives);
		run(fadeOut);
		run(new Hardboiled(font, parent));
	}

	private function showSorry(dt:Float):Bool {
		if (title == null) {
			parent.removeChildren();

			final bigFont = hxd.Res.AmaticSC.toSdfFont(100, SDFChannel.MultiChannel, 0.5, 4 / 48);
			if (title != null) {
				title.remove();
			}
			title = new Text(bigFont, parent);
			title.setPosition(parent.width / 2, parent.height / 2 - 75);
			title.textColor = 0xffffffff;
			title.letterSpacing = 0;
			title.smooth = true;
			title.textAlign = Align.Center;
			title.text = "Sorry!";
			title.alpha = 0;
			title.addShader(new Doodle());
		}
		title.alpha += dt / 2;
		if (title.alpha >= 1) {
			return false;
		}
		return true;
	}

	final firstTime:Bool;
	var informant:Actor;
	var uma:Actor;
	var umaLight:AtlasBitmap;
	var xDiff:Float;
	var title:Text;
}
