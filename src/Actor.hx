import h2d.Anim;
import h2d.Flow;
import h2d.Object;
import hxd.Res;

class Actor extends Anim {
	public var currentAnim:String;
	public var width(get, null):Float;
	public var height(get, null):Float;
	public var dx(get, null):Float;
	public var dy(get, null):Float;
	public var dir(get, null): Int;

	public function new(name:String, ?parent:Object) {
		super([], 5, parent);
		prefix = name + '_';
		Main.ME.onReloadAtlas(replay);
	}

	private function replay() {
		if (currentAnim != null) {
			playAnim(currentAnim, currentFrame);
		}
	}

	public function playAnim(name:String, atFrame = 0.0) {
		currentAnim = name;
		final frames = anim(prefix + name);
		play(frames, Math.min(frames == null ? 0 : frames.length, atFrame));
		if (xFlip) {
			flipX();
			xFlip = true;
		}
	}

	private static function anim(name:String) {
		return Res.tiles.getAnim(name, FlowAlign.Middle, FlowAlign.Bottom);
	}

	public function flipX() {
		xFlip = !xFlip;
		for (frame in frames) {
			if (frame == null) continue;
			frame.flipX();
		}
	}

	private function get_width() {
		return frames.length > 0 ? frames[0].width : 0.0;
	}

	private function get_height() {
		return frames.length > 0 ? frames[0].height : 0.0;
	}

	private function get_dx() {
		return frames.length > 0 ? frames[0].dx : 0.0;
	}

	private function get_dy() {
		return frames.length > 0 ? frames[0].dy : 0.0;
	}

	function get_dir() {
		return xFlip ? -1 : 1;
	}

	final prefix:String;
	var xFlip = false;
}
