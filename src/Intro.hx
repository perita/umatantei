import h2d.Font;
import h2d.Interactive;
import h2d.Text;
import hxd.Event;
import hxd.Res;

class Intro extends Stage {
	override function setup(dt:Float):Bool {
		super.setup(dt);

		bartenderLight = new AtlasBitmap("bartender_light", root);
		setPosition(bartenderLight, 30, 90);
		setPosition(bartenderLight, 30, 90);
		bartenderLight.visible = false;

		bartender = new Actor("bartender", bartenderLight);
		bartender.playAnim("cleaning");
		bartender.setPosition(5, -82);

		setPosition(new AtlasBitmap("bartender_bar", root), 0, 185);

		umaLight = new AtlasBitmap("umatantei_light", root);
		setPosition(umaLight, 3 * parent.width / 4, 90);
		umaLight.visible = false;

		glass = new Actor("bartender", umaLight);
		glass.playAnim("glass");
		glass.setPosition(-52 + glass.width / 2, -154 + glass.height);
		glass.visible = false;

		interactive = new Interactive(glass.width + 40, glass.height + 40, glass);
		interactive.setPosition(-20 + glass.dx, -20 + glass.dy);
		interactive.visible = false;
		interactive.onPush = continueScene;

		uma = new Actor("umatantei", umaLight);
		uma.playAnim("standing");
		uma.flipX();
		uma.setPosition(-5, -15);

		createFade();

		hxd.snd.Manager.get().stopAll();
		Res.fast_talking.play(true);

		run(fadeIn);
		run(lightUma);
		wait(1);
		run(walkToTheBar);
		wait(0.5);
		umaSay("Pour me a whisky, Lloyd");
		wait(0.5);
		run(lightBartender);
		run(pour);
		wait(0.5);
		bartenderSay("Why the long face, Detective Horse?");
		wait(1);
		umaSay("Things are starting to get tight:");
		umaSay("I’m low on cash.");
		umaSay("Customers are hard to come by.");
		umaSay("I have’nt had a breakthrought\nin the chicken case yet.");
		umaSay("And I’m three months\nbehind on alimony.");
		wait(1);
		umaSay("My glue addiction broke\nmy family apart.");
		wait(1);
		bartenderSay("I thought glue usually\nbrought things together.");
		wait(1);
		umaSay("Also, I’m a horse, so...");
		run(enableInteractive);
		run(returnControl);

		return false;
	}

	private function lightUma(dt:Float):Bool {
		return switchOn(umaLight);
	}

	private function switchOn(light:AtlasBitmap) {
		Res.switch_on.play(false);
		light.visible = true;
		return false;
	}

	private function lightBartender(dt:Float):Bool {
		return switchOn(bartenderLight);
	}

	private function pour(dt:Float):Bool {
		if (bartender.currentAnim == "cleaning" && Std.int(bartender.currentFrame) == 14) {
			bartender.loop = false;
			bartender.playAnim("pour");
			prevOnAnimEnd = bartender.onAnimEnd;
			bartender.onAnimEnd = () -> {
				bartender.onAnimEnd = prevOnAnimEnd;
				end = true;
			}
		} else if (!glass.visible && bartender.currentAnim == "pour" && Std.int(bartender.currentFrame) == 20) {
			glass.visible = true;
		} else if (end) {
			bartender.loop = true;
			bartender.playAnim("cleaning", 15);
			end = false;
			return false;
		}
		return true;
	}

	private function walkToTheBar(dt:Float):Bool {
		if (uma.currentAnim != "walking") {
			uma.playAnim("walking");
		}
		umaLight.x += -150 * dt;
		if (umaLight.x > 110 + umaLight.tile.width / 2) {
			return true;
		}
		uma.playAnim("standing");
		return false;
	}

	private function grabWhisky(dt:Float):Bool {
		if (uma.currentAnim != "grab" && glass.currentAnim != "falling") {
			uma.loop = false;
			uma.playAnim("grab");
			prevOnAnimEnd = uma.onAnimEnd;
			uma.onAnimEnd = () -> {
				uma.onAnimEnd = prevOnAnimEnd;
				uma.loop = true;
				uma.playAnim("standing");
			}
		}
		if (glass.currentAnim != "falling" && Std.int(uma.currentFrame) == 3) {
			playing = false;
			glass.loop = false;
			glass.playAnim("falling");
			glass.setPosition(-110 + glass.width / 2, -168 + glass.height);
			prevOnAnimEnd = glass.onAnimEnd;
			glass.onAnimEnd = () -> {
				glass.onAnimEnd = prevOnAnimEnd;
				glass.remove();
				end = true;
			}
		}
		if (!playing && glass.currentAnim == "falling" && Std.int(glass.currentFrame) == 7) {
			playing = true;
			Res.breaking_glass.play(false);
		}
		if (end) {
			end = false;
			playing = false;
			return false;
		}
		return true;
	}

	private function umaSay(line:String) {
		var speech:Speech = null;
		run((dt) -> {
			if (speech == null) {
				speech = new Speech(font, umaLight.x, umaLight.y + uma.y - uma.height, line, parent);
			}
			return speech.visible;
		});
	}

	private function bartenderSay(line:String) {
		var speech:Speech = null;
		run((dt) -> {
			if (speech == null) {
				speech = new Speech(font, bartenderLight.x, bartenderLight.y + bartender.y - bartender.height + 10, line, parent);
			}
			return speech.visible;
		});
	}

	private function enableInteractive(dt:Float):Bool {
		interactive.visible = true;
		return false;
	}

	private function continueScene(e:Event) {
		interactive.remove();
		run(grabWhisky);
		umaSay("Got no opposable thumbs.");
		wait(0.5);
		umaSay("In fact, no fingers\nwhatsoever.");
		bartenderSay("There, there.");
		bartenderSay("Things are bound to get better.");
		bartenderSay("Tough times are only\ntemporary.");
		run(fadeOut);
		wait(1);
		bartenderSay("That’ll be $20.");
		umaSay("Darn!");
		run(showTitle);
	}

	private function showTitle(dt:Float):Bool {
		if (title == null) {
			parent.removeChildren();

			final bigFont = hxd.Res.AmaticSC.toSdfFont(100, SDFChannel.MultiChannel, 0.5, 4 / 48);
			if (title != null) {
				title.remove();
			}
			title = new Text(bigFont, parent);
			title.setPosition(parent.width / 2, 0);
			title.textColor = 0xffffffff;
			title.letterSpacing = 0;
			title.smooth = true;
			title.textAlign = Align.Center;
			title.text = "Detective Horse";
			title.addShader(new Doodle());

			final subtitle = new Text(font, title);
			subtitle.setPosition(0, title.textHeight);
			subtitle.textColor = 0xffffffff;
			subtitle.letterSpacing = 0;
			subtitle.smooth = true;
			subtitle.textAlign = Align.Center;
			subtitle.text = "The case of the murdered chicken";
			subtitle.addShader(new Doodle());

			title.alpha = 0;
		}
		title.alpha += dt / 2;
		if (title.alpha >= 1) {
			interactive = new Interactive(parent.width, parent.height, parent);
			interactive.onPush = startGame;
			return false;
		}
		return true;
	}

	private function startGame(e:Event) {
		interactive.cursor = hxd.Cursor.Default;
		interactive.onPush = nopEventHandler;
		run(hideTitle);
	}

	private function hideTitle(dt:Float):Bool {
		title.alpha -= dt / 2;
		if (title.alpha <= 0) {
			interactive.remove();
			title.remove();
			title = null;
			run(new Informant(font, parent));
			return false;
		}
		return true;
	}

	static function nopEventHandler(e:Event) {}

	var bartenderLight:AtlasBitmap;
	var umaLight:AtlasBitmap;
	var bartender:Actor;
	var uma:Actor;
	var glass:Actor;
	var interactive:Interactive;
	var end = false;
	var playing = false;
	var prevOnAnimEnd = null;
	var title:Text;
}
