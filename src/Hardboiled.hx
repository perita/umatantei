import h2d.Interactive;
import hxd.Event;

class Hardboiled extends Stage {
	override function setup(dt:Float):Bool {
		super.setup(dt);

		final street = new AtlasBitmap("hardboiled_street", root);
		street.setPosition(parent.width / 2, street.tile.height + 3);

		final prevArrow = new AtlasBitmap("hardboiled_prev");
		final arrowInteractive = newInteractive(prevArrow.tile.width, prevArrow.tile.height);
		arrowInteractive.setPosition(10, 10);
		arrowInteractive.onPush = prevStage;
		arrowInteractive.visible = false;
		arrowInteractive.addChild(prevArrow);
		prevArrow.setPosition(-prevArrow.dx, -prevArrow.dy);

		umaLight = setPosition(new AtlasBitmap("umatantei_light", root), 25, 3);

		hardboiled = new Actor("hardboiled", root);
		hardboiled.playAnim("walking");
		setPosition(hardboiled, 132, 165);

		hardboiledInter = newInteractive(hardboiled.width, hardboiled.height, hardboiled);
		hardboiledInter.setPosition(hardboiled.dx, hardboiled.dy);
		hardboiledInter.visible = false;
		hardboiledInter.onPush = talkToHardboiled;

		trafficLights = new Actor("hardboiled", root);
		trafficLights.speed = 2;
		trafficLights.playAnim("traffic_lights");
		setPosition(trafficLights, 444, 25);

		uma = new Actor("umatantei", root);
		uma.playAnim("standing");
		setPosition(uma, 70, 58);
		xDiff = umaLight.x - uma.x - uma.dx;
		uma.x = -uma.width - 10;
		syncLight();

		createFade();

		runP(move);
		run(fadeIn);
		run(walkFromEast);
		run(enableInteractives);

		return false;
	}

	function prevStage(e:Event) {
		run(walkToEast);
		run(disableInteractives);
		run(fadeOut);
		run(new Informant(font, parent, false));
	}

	function walkFromEast(dt:Float):Bool {
		return walkTo(dt, uma.width + 10);
	}

	function walkToEast(dt:Float):Bool {
		return walkTo(dt, -uma.width - 10);
	}

	function move(dt:Float):Bool {
		if (hardboiled.currentAnim == "walking") {
			hardboiled.x += 150 * dt * hardboiled.dir;
			if (hardboiled.dir > 0 && hardboiled.x >= 552 || hardboiled.dir < 0 && hardboiled.x <= 132) {
				hardboiled.playAnim("standing");
				waiting = 2;
			}
		} else if (waiting > 0) {
			waiting -= dt;
			if (waiting <= 0) {
				if (hardboiled.dir > 0 && hardboiled.x >= 552 || hardboiled.dir < 0 && hardboiled.x <= 132) {
					hardboiled.flipX();
				}
				hardboiled.playAnim("walking");
			}
		}
		return root.parent != null;
	}

	function walkTo(dt:Float, x:Float):Bool {
		if (Math.abs(uma.x - x) < 5) {
			uma.playAnim("standing");
			return false;
		}
		if (uma.currentAnim != "walking") {
			uma.playAnim("walking");
			if (uma.x < x && uma.dir < 0 || uma.x > x && uma.dir > 0) {
				uma.flipX();
			}
		}
		uma.x += 150 * dt * uma.dir;
		syncLight();
		return true;
	}

	inline function syncLight() {
		umaLight.x = uma.x + uma.dx + xDiff + (uma.dir < 0 ? 10 : 0);
	}

	function talkToHardboiled(e:Event) {
		waiting = 0;
		hardboiled.playAnim("standing");
		disableInteractives(0);

		run(walkToHardboiled);
		run(faceHardboiled);
		if (Main.ME.state.talkToHardboiled) {
			eggSay("Please, talk to my partner.");
		} else {
			umaSay("Have you heard about the chicken?");
			eggSay("Chicken?");
			wait(0.5);
			eggSay("No, it doesn’t ring a bell.");
			wait(0.5);
			eggSay("What are you, an ass?");
			umaSay("No, I’m a horse.");
			eggSay("...");
			eggSay("Look, I don’t have time to\nchit-chat, Mister Ed.");
			umaSay("Name’s Detective Horse.");
			eggSay("...");
			eggSay("As I was saying,\nI don’t have time.");
			eggSay("Talk to my partner, the Sardinian,\nand see if he can help you.");
		}
		run(resumeWalking);
		run(enableInteractives);
		run(returnControl);

		Main.ME.state.talkToHardboiled = true;
	}

	function walkToHardboiled(dt:Float):Bool {
		return walkTo(dt, hardboiled.x - hardboiled.width - 10);
	}

	function resumeWalking(dt:Float):Bool {
		hardboiled.playAnim("walking");
		return false;
	}

	function faceHardboiled(dt:Float):Bool {
		if (uma.dir < 0) {
			uma.flipX();
		}
		if (hardboiled.dir > 0) {
			hardboiled.flipX();
		}
		return false;
	}

	function umaSay(line:String) {
		run((dt) -> say(uma.x, uma.y - uma.height + 30, line));
	}

	function eggSay(line:String) {
		run((dt) -> say(hardboiled.x, hardboiled.y - hardboiled.height, line));
	}

	var trafficLights:Actor;
	var uma:Actor;
	var hardboiledInter:Interactive;
	var hardboiled:Actor;
	var umaLight:AtlasBitmap;
	var xDiff:Float;
	var waiting = 0.0;
}
