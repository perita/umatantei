import h2d.Bitmap;
import h2d.Font;
import h2d.Interactive;
import h2d.Object;
import h2d.Scene;
import hxd.Res;

abstract class Stage {
	public function new(font:Font, parent:Scene) {
		this.font = font;
		this.parent = parent;
	}

	public function setup(dt:Float):Bool {
		parent.removeChildren();
		root = new Object(parent);
		return false;
	}

	function newInteractive(width:Float, height:Float, ?parent:h2d.Object):Interactive {
		final interactive = new Interactive(width, height, parent == null ? root : parent);
		interactives.push(interactive);
		return interactive;
	}

	function enableInteractives(dt:Float):Bool {
		for (interactive in interactives) {
			interactive.cursor = hxd.Cursor.Button;
			interactive.visible = true;
		}
		return false;
	}

	function disableInteractives(dt:Float):Bool {
		for (interactive in interactives) {
			interactive.cursor = hxd.Cursor.Default;
			interactive.visible = false;
		}
		return false;
	}

	private function fadeIn(dt:Float):Bool {
		fade.alpha -= dt / 2;
		return fade.alpha > 0;
	}

	private function fadeOut(dt:Float):Bool {
		fade.alpha += dt;
		return fade.alpha < 1;
	}

	function returnControl(dt:Float):Bool {
		if (control == null) {
			Res.click.play(false);
			control = new AtlasBitmap("umatantei_control", parent);
			control.setPosition(parent.width / 2, parent.height / 2);
			controlToggle = 0.5;
			controlLoops = 3;
		}
		controlToggle -= dt;
		if (controlToggle <= 0) {
			controlLoops--;
			if (controlLoops == 0) {
				control.remove();
				control = null;
				return false;
			}
			control.visible = !control.visible;
			controlToggle += 0.5;
		}
		return true;
	}

	function say(x:Float, y:Float, line:String):Bool {
		if (speech == null) {
			speech = new Speech(font, x, y, line, parent);
		}
		if (speech.visible) {
			return true;
		}
		speech.remove();
		speech = null;
		return false;
	}

	private function wait(seconds:Float) {
		run((dt:Float) -> {
			seconds -= dt;
			return seconds > 0;
		});
	}

	overload extern inline function run(f:(Float) -> Bool) {
		Main.ME.run(f);
	}

	overload extern inline function run(stage:Stage) {
		Main.ME.run(stage);
	}

	overload extern inline function runP(f:(Float) -> Bool) {
		Main.ME.runP(f);
	}

	overload extern inline function setPosition(bitmap:AtlasBitmap, x:Float, y:Float):AtlasBitmap {
		bitmap.setPosition(x + bitmap.tile.width / 2, y + bitmap.tile.height);
		return bitmap;
	}

	overload extern inline function setPosition(actor:Actor, x:Float, y:Float):Actor {
		actor.setPosition(x + actor.width / 2, y + actor.height);
		return actor;
	}

	function createFade() {
		fade = new Bitmap(h2d.Tile.fromColor(0x000000, Main.ME.engine.width, Main.ME.engine.height), parent);
	}

	final font:Font;
	final parent:Scene;
	var root:Object;
	var fade:Bitmap;
	var control:AtlasBitmap = null;
	var controlLoops = 3;
	var controlToggle = 0.5;
	var speech:Speech = null;
	final interactives:Array<Interactive> = [];
}
