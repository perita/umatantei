class Doodle extends hxsl.Shader {
	static var SRC = {
		@:import h3d.shader.Base2d;
		function vertex() {
			var t = snap(time, 0.25);
			var noise:Vec2 = random3(vec3(input.position.xy + t, 1)).xy * 0.005;
			output.position.xy += noise;
		}
		function snap(x:Float, snap:Float):Float {
			return snap * floor(x / snap);
		}
		// https://www.shadertoy.com/view/XsX3zB
		function random3(c:Vec3):Vec3 {
			var j:Float = 4096.0 * sin(dot(c, vec3(17.0, 59.4, 15.0)));
			var r:Vec3;
			r.z = fract(512.0 * j);
			j *= .125;
			r.x = fract(512.0 * j);
			j *= .125;
			r.y = fract(512.0 * j);
			return r - 0.5;
		}
	};
}
