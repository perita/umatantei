import h2d.Flow;
import hxd.Res;

class AtlasBitmap extends h2d.Bitmap {
	public var dx(get, null):Float;
	public var dy(get, null):Float;

	public function new(tileName:String, ?parent:h2d.Object) {
		super(getTile(tileName), parent);
		this.tileName = tileName;
		Main.ME.onReloadAtlas(reload);
	}

	private static function getTile(name:String) {
		return Res.tiles.get(name, FlowAlign.Middle, FlowAlign.Bottom);
	}

	private function reload() {
		tile = getTile(tileName);
	}

	function get_dx() {
		return tile != null ? tile.dx : 0.0;
	}

	function get_dy() {
		return tile != null ? tile.dy : 0.0;
	}


	final tileName:String;
}
