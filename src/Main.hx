import hxd.Window;
import h2d.Font;
import hxd.Res;

class Main extends hxd.App {
	public static var ME:Main;
	
	public final state:State = {
		talkToHardboiled: false,
		talkToSardinian: false,
	};

	public static function main() {
		new Main();
	}

	public function new() {
		ME = this;
		super();
	}

	override function loadAssets(onLoaded:() -> Void) {
		#if js
		Res.initEmbed();
		#else
		Res.initLocal();
		#end
		onLoaded();
	}

	override function init() {
		engine.backgroundColor = 0xff000000;
		s2d.scaleMode = LetterBox(667, 375, true, Center, Center);
		final font = hxd.Res.AmaticSC.toSdfFont(42, SDFChannel.MultiChannel, 0.5, 4 / 24);
		Res.tiles.watch(reloadAtlas);
		Window.getInstance().onClose = () -> {
			dispose();
			return true;
		};

		#if debug
		// run(new Informant(font, s2d));
		run(new Hardboiled(font, s2d));
		#else
		run(new Intro(font, s2d));
		#end
	}

	private function reloadAtlas() {
		@:privateAccess Res.tiles.contents = null;
		for (callback in reloadAtlasCallbacks) {
			callback();
		}
	}

	public function onReloadAtlas(callback:() -> Void) {
		reloadAtlasCallbacks.push(callback);
	}

	overload extern inline public function run(action:(Float) -> Bool) {
		actions.add(action);
	}

	overload extern inline public function run(stage:Stage) {
		actions.add(stage.setup);
	}

	overload extern inline public function runP(action:(Float) -> Bool) {
		actionsP.add(action);
	}

	override function update(dt:Float) {
		for (action in actionsP) {
			if (!action(dt)) {
				actionsP.remove(action);
			}
		}
		while (!actions.isEmpty()) {
			final action = actions.first();
			if (action(dt)) {
				break;
			}
			actions.pop();
		}
		#if debug
		if (hxd.Key.isPressed(hxd.Key.R)) {
			final mute = isMuted();
			dispose();
			final main = new Main();
			if (mute) {
				main.toggleMute();
			}
		}
		if (hxd.Key.isPressed(hxd.Key.M)) {
			toggleMute();
		}
		#end
	}

	override function dispose() {
		hxd.snd.Manager.get().dispose();
		super.dispose();
	}

	private function toggleMute() {
		return hxd.snd.Manager.get().masterChannelGroup.mute = !isMuted();
	}

	private function isMuted() {
		return hxd.snd.Manager.get().masterChannelGroup.mute;
	}

	final actions:List<(Float) -> Bool> = new List();
	final actionsP:List<(Float) -> Bool> = new List();
	final reloadAtlasCallbacks:Array<() -> Void> = new Array();
}
