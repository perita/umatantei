import h2d.Bitmap;
import h2d.Interactive;
import h2d.Text;
import h2d.ScaleGrid;
import h2d.Font;
import h2d.Scene;

class Speech extends Interactive {
	public function new(font:Font, x:Float, y:Float, line:String, parent:Scene) {
		super(parent.width, parent.height, parent);
		cursor = hxd.Cursor.Default;

		final bubble = new ScaleGrid(hxd.Res.tiles.get("umatantei_speech_bubble"), 32, 32, 32, 32, this);
		final tail = new Bitmap(hxd.Res.tiles.get("umatantei_speech_tail"), this);

		final text = new Text(font, bubble);
		text.setPosition(32, 0);
		text.textColor = 0xffffffff;
		text.letterSpacing = 0;
		text.smooth = true;
		text.textAlign = Align.Center;
		text.text = line;

		bubble.width = text.textWidth + 64;
		text.x = bubble.width / 2;
		bubble.height = Math.max(64, text.textHeight + 12);
		text.y = bubble.height / 2 - text.textHeight / 2 - 6;
		bubble.setPosition(Math.max(15, Math.min(x - bubble.width / 2, width - bubble.width)), y - bubble.height - tail.tile.height);
		tail.setPosition(x - tail.tile.width / 2, y - tail.tile.height - 2);
	}

	override function onPush(e:hxd.Event) {
		visible = false;
		remove();
	}
}
